# Python exercises

These are Python exercises I made for fun and to better myself. The most useful are the site blocker I made myself and the password md5 cracker

## Installation

Just download them. If you have any missing libraries, just run "pip install library_name" on a terminal or command prompt

The file with the most libraries needed is block_site_gui.py. Use Visual Studio Code to check for missing libraries

Examples:
```bash
pip install tkinter
```

## Usage

This was meant to be use in Python3, you can download it from the official Python site

```python3
python3 name_of_py.py
```

## Contributing
Anybody can copy and use the codes provided here, just credit the source code and that is all

## License
[GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)