import tkinter as tk
from tkinter import *
from tkinter import messagebox
from datetime import datetime  
import platform
from time import sleep
import os
from subprocess import call
import block_app

#Title
root = tk.Tk()
root.title('Site blocker')

operating = platform.system()
os_linux = "Linux"
current_time = datetime.now()

#This is the localhost, aka it causes the OS to navigate to itself, causing an error
redirect = "127.0.0.1"

# specify size of window.
root.geometry("400x250")
  # Create widgets specify size.

def helping():
    messagebox.showinfo(message="Press open hostfile button to see the host file. Then put the websites you want to block or unblock on the textbox and press Block/Unblock. If you want to block an app, it will open a commandline", title="HELP")

b4 = Button(root, text = "HELP ", command=helping)
b4.pack(side=tk.TOP)
above_time = Label(root, text = "The current time is: ")
current_time = Label(root, text = current_time)
these_labels = Label(root, text = "Put the sites you want to block, separated by spaces")
end_time = datetime(2021, 8, 31, 23)
site_lol = Text(root, height = 2, width = 100) 

nombre_os = os.name
verificar_linux = "posix"
verificar_nt = "nt"

def blocksite():
    if operating == os_linux:
        
        blocked_sites = "/etc/hosts"
        with open(blocked_sites, 'r') as blocked_sites:
            lines = blocked_sites.read()
            messagebox.showinfo(message=lines, title="Data in host file")

        #site = str(raw_input("Put the sites here: "))
        site = site_lol.get("1.0",'end-1c')
        sites_to_block = str.split(site)
        #Get the OS name, then run accordingly
        hosts_path = "/etc/hosts"

        #If the current date is inferior to the blocking date specified, it blocks the site
        if datetime.now() < end_time:
            #opens the hostfile using a read and write access and writes everything in a for loop
            with open(hosts_path, 'r+') as hostfile:
                hosts_content = hostfile.read()
                for site in sites_to_block:
                    if site not in hosts_content:
                        hostfile.write(redirect + " " + site + "\n")
                messagebox.showinfo(message=site, title="Site blocked")
        #If the current date is superior, it unblocks the site
        else:
            #Opens the hostfile using a read and write access and deletes everything in a for loop
            with open(hosts_path, 'r+') as hostfile:
                lines = hostfile.readlines()
                hostfile.seek(0)
                for line in lines:
                    if not any(site in line for site in sites_to_block):
                        hostfile.write(line)
                hostfile.truncate()
                messagebox.showinfo(message=line, title="Site unblocked")

   #Windows fallback if the OS is not Linux
    else:

        blocked_sites = "C:\Windows\System32\drivers\etc\hosts"
        with open(blocked_sites, 'r') as blocked_sites:
            lines = blocked_sites.read()
            messagebox.showinfo(message=lines, title="Data in hostfile")

        #site = str(raw_input("Put the sites here: "))
        site = site_lol.get("1.0",'end-1c')
        sites_to_block = str.split(site)
        hosts_path = "C:\Windows\System32\drivers\etc\hosts"
        #If the current date is inferior to the blocking date specified, it blocks the site
        if datetime.now() < end_time:
            messagebox.showinfo(message=site, title="Site blocked")
            #opens the hostfile using a read and write access and writes everything in a for loop
            with open(hosts_path, 'r+') as hostfile:
                hosts_content = hostfile.read()
                for site in sites_to_block:
                    if site not in hosts_content:
                        hostfile.write(redirect + " " + site + "\n")
        #If the current date is superior, it unblocks the site
        else:
            messagebox.showinfo(message=site, title="Site unblocked")
            #Opens the hostfile using a read and write access and deletes everything in a for loop
            with open(hosts_path, 'r+') as hostfile:
                lines = hostfile.readlines()
                hostfile.seek(0)
                for line in lines:
                    if not any(site in line for site in sites_to_block):
                        hostfile.write(line)
                hostfile.truncate()

def open_hostfile():
    if operating == os_linux:
        le_sites = "/etc/hosts"
        with open(le_sites, 'r') as blocked_sites:
            lines = blocked_sites.read()
            messagebox.showinfo(message=lines, title="Hostfile content")

    else:
        le_sites = "C:\Windows\System32\drivers\etc\hosts"
        with open(le_sites, 'r') as blocked_sites:
            lines = blocked_sites.read()
            messagebox.showinfo(message=lines, title="Hostfile content")

def blockprogram():
    block_app.killing()

b2 = Button(root, text = "Block/Unblock sites ", command=blocksite)
b3 = Button(root, text = "Open the hostfile ", command=open_hostfile)
b5 = Button(root, text = "Block program ", command=blockprogram)

above_time.pack()
current_time.pack()
these_labels.pack()
site_lol.pack()
b2.pack(side=tk.BOTTOM)      # pack starts packing widgets on the left 
b3.pack(side=tk.BOTTOM)
b5.pack(side=tk.BOTTOM)
tk.mainloop()