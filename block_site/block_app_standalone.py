import os
import sys
import subprocess #This is for Linux
from time import sleep
import psutil #This is for Windows
from os import system #Os Commands

nombre_os = os.name
verificar_linux = "posix"
verificar_nt = "nt"

print("This will block an app from starting up every five seconds \n")
print("To stop this you need to use task manager and kill Python3 or use the terminal and kill Python3")
print("DO NOT PUT TASK MANAGER OR TERMINAL, OR YOU WILL BE STUCK \n")

if nombre_os == "posix":
    target_process = str(input("Put the app's name here, if it is a Windows executable, put a .exe: "))
    while True:
        sleep(5)
        todos = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE) #This will show every Linux process
        output, error = todos.communicate()
        print(output, "\n") #This will print every process
        sleep(10)
        for line in output.splitlines():
            print("STATE  PID        START_TIME  NAME")
            print(line) #Process it is currently at
            sleep(0.5)
            if target_process in str(line):
                pid = int(line.split(None, 1)[0]) #If it was found, it splits off the PID
                os.kill(pid, 9) #9 means that the OS itself forces the program to close
                print("The process was stopped \n")
                sleep(10)
                system('clear')
            else:
                print("Process not found yet \n") #If it didn't find the process, throws this line
                sleep(0.05)
                system('clear')

elif nombre_os == "nt":
    target_process = str(input("Put the app's name here, if it is a Windows executable, put a .exe: "))
    while True:
        sleep(5) #Iterates every process by pid
        for p in psutil.process_iter(attrs=['pid', 'name']): 
            print(p) #This will print the process it is at right now
            sleep(0.5)
            if p.info['name'] == target_process:
                print("Process found")
                sleep(5)
                os.system("taskkill /f /im " + target_process) #Forces the process to crash using its name
                print("The process was stopped \n")
                sleep(5)
                system('cls')
            else:
                print("Process not found yet \n")
                sleep(0.05)
                system('cls')