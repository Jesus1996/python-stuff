import tkinter as tk
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
root = tk.Tk()

#Title
root.title('Text reader')

# specify size of window.
root.geometry("250x170")
  
# Create text widget and specify size.
T = Text(root, height = 5, width = 52)

def browseFiles():
    filename = filedialog.askopenfilename(initialdir = "/",
                                          title = "Select a File",
                                          filetypes = (("Text files",
                                                        "*.txt*"),
))
# datos = open(filename)
# Equalize the filename to the text
    with open(filename, "r") as f:
        valid_utf8 = True
        try:
            T.insert(1.0, f.read())
        except UnicodeDecodeError:
            valid_utf8 = False
            messagebox.showerror(message="The format is not utf-8", title="Error")


# Create label
l = Label(root, text = "Text you want to display")
  
# Create an Exit button.
b1 = Button(root, text = "Exit",
            command = root.destroy) 

b2 = Button(root, text = "Search text", command=browseFiles) 
  
l.pack()
T.pack()
b2.pack()
b1.pack()
  
tk.mainloop()