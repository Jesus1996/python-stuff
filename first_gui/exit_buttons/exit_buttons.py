import tkinter as tk   
from tkinter import messagebox

def write_text():
    messagebox.showinfo(message="This is a message box", title="This is a title")

#Creates the window itself
parent = tk.Tk()
frame = tk.Frame(parent)

#Title
parent.title('Text reader')
frame.pack()

#This is one of the buttons
text_disp= tk.Button(frame, 
                   text="Hello", 
                   command=write_text
                   )

text_disp.pack(side=tk.LEFT)

exit_button = tk.Button(frame,
                   text="Exit",
                   fg="green",
                   command=quit)
exit_button.pack(side=tk.RIGHT)

parent.mainloop()