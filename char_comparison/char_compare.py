import tkinter as tk
from tkinter import *
from tkinter import messagebox
import hashlib
import time
from time import sleep

#Title
root = tk.Tk()
root.title('CHAR COMPARISON')
root.geometry("600x400")
root.resizable(False, False)

#chars
char_a_name = Label(root, text = "Name of the first character")
char_b_name = Label(root, text = "Name of the second character")
char_a_name_input = Text(root, height = 1, width = 30)
char_b_name_input = Text(root, height = 1, width = 30)

#char a stuff
char_a_attack_potency_label = Label(root, text = "Attack Potency in joules")
char_a_attack_potency_input = Text(root, height = 1, width = 30)

char_a_speed_label = Label(root, text = "Speed in meters per second")
char_a_speed_input = Text(root, height = 1, width = 30)

char_a_reaction_label = Label(root, text = "Reaction Speed in meters per second")
char_a_reaction_input = Text(root, height = 1, width = 30)

char_a_lifting_label = Label(root, text = "Lifting Strength in kilograms")
char_a_lifting_input = Text(root, height = 1, width = 30)

char_a_striking_label = Label(root, text = "Striking Strength in joules")
char_a_striking_input = Text(root, height = 1, width = 30)


#char b stuff
char_b_attack_potency_label = Label(root, text = "Attack Potency in joules")
char_b_attack_potency_input = Text(root, height = 1, width = 30)

char_b_speed_label = Label(root, text = "Speed in meters per second")
char_b_speed_input = Text(root, height = 1, width = 30)

char_b_reaction_label = Label(root, text = "Reaction Speed in meters per second")
char_b_reaction_input = Text(root, height = 1, width = 30)

char_b_lifting_label = Label(root, text = "Lifting Strength in kilograms")
char_b_lifting_input = Text(root, height = 1, width = 30)

char_b_striking_label = Label(root, text = "Striking Strength in joules")
char_b_striking_input = Text(root, height = 1, width = 30)

char_a_durability_label = Label(root, text = "Durability in joules")
char_a_durability_input = Text(root, height = 1, width = 30)

char_b_durability_label = Label(root, text = "Durability in joules")
char_b_durability_input = Text(root, height = 1, width = 30)


def results():
    # \n es el final de la linea
    if char_a_name_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The name of character A is empty", title="Error")
        return

    if char_b_name_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The name of character B is empty", title="Error")
        return

    if char_a_attack_potency_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The attack potency of character A is empty", title="Error")
        return

    if char_b_attack_potency_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The attack potency of character b is empty", title="Error")
        return

    if char_a_speed_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The speed of character a is empty", title="Error")
        return

    if char_b_speed_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The speed of character b is empty", title="Error")
        return

    if char_a_lifting_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The lifting strength of character a is empty", title="Error")
        return

    if char_b_lifting_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The lifting strength of character b is empty", title="Error")
        return

    if char_a_reaction_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The reaction speed of character a is empty", title="Error")
        return

    if char_b_reaction_input .get("1.0", END)=="\n":
        messagebox.showerror(message="The reaction speed of character b is empty", title="Error")
        return

    if char_a_striking_input.get("1.0", END)=="\n":
        messagebox.showerror(message="The striking strength of character a is empty", title="Error")
        return

    if char_b_striking_input .get("1.0", END)=="\n":
        messagebox.showerror(message="The striking strength of character b is empty", title="Error")
        return

    if char_a_durability_input .get("1.0", END)=="\n":
        messagebox.showerror(message="The durability of character a is empty", title="Error")
        return

    if char_b_durability_input .get("1.0", END)=="\n":
        messagebox.showerror(message="The durability of character b is empty", title="Error")
        return

    char_a_attack_potency_input_converted = char_a_attack_potency_input.get("1.0",END)
    char_a_attack_potency_input_converted_2 = char_a_attack_potency_input_converted.replace(",", "")
    char_a_speed_input_converted = char_a_speed_input.get("1.0",END)
    char_a_speed_input_converted_2 = char_a_speed_input_converted.replace(",", "")
    char_a_reaction_input_converted = char_a_reaction_input.get("1.0",END)
    char_a_reaction_input_converted_2 = char_a_reaction_input_converted.replace(",", "")
    char_a_striking_input_converted = char_a_striking_input.get("1.0",END)
    char_a_striking_input_converted_2 = char_a_striking_input_converted.replace(",","")
    char_a_lifting_input_converted = char_a_lifting_input.get("1.0",END)
    char_a_lifting_input_converted_2 = char_a_lifting_input_converted.replace(",","")
    char_a_durability_converted = char_a_durability_input.get("1.0",END)
    char_a_durability_input_converted_2 = char_a_durability_converted.replace(",","")

    char_b_attack_potency_input_converted = char_b_attack_potency_input.get("1.0",END)
    char_b_attack_potency_input_converted_2 = char_b_attack_potency_input_converted.replace(",", "")
    char_b_speed_input_converted = char_b_speed_input.get("1.0",END)
    char_b_speed_input_converted_2 = char_b_speed_input_converted.replace(",", "")
    char_b_reaction_input_converted = char_b_reaction_input.get("1.0",END)
    char_b_reaction_input_converted_2 = char_b_reaction_input_converted.replace(",", "")
    char_b_striking_input_converted = char_b_striking_input.get("1.0",END)
    char_b_striking_input_converted_2 = char_b_striking_input_converted.replace(",","")
    char_b_lifting_input_converted = char_b_lifting_input.get("1.0",END)
    char_b_lifting_input_converted_2 = char_b_lifting_input_converted.replace(",","")
    char_b_durability_converted = char_b_durability_input.get("1.0",END)
    char_b_durability_input_converted_2 = char_b_durability_converted.replace(",","")


    result_above_attack = float(char_a_attack_potency_input_converted_2) - float(char_b_attack_potency_input_converted_2)
    result_above_attack_2 = abs(result_above_attack)

    result_below_attack = (float(char_a_attack_potency_input_converted_2) + float(char_b_attack_potency_input_converted_2))/2
    result_below_attack_2 = abs(result_below_attack)

    final_result_ap = result_above_attack_2/result_below_attack_2
    percentage_final_ap = 100 * final_result_ap



    result_above_speed = float(char_a_speed_input_converted_2) - float(char_b_speed_input_converted_2)
    result_above_speed_2 = abs(result_above_speed)

    result_below_speed = (float(char_a_speed_input_converted_2) + float(char_b_speed_input_converted_2))/2
    result_below_speed_2 = abs(result_below_speed)

    final_result_speed = result_above_speed_2/result_below_speed_2
    percentage_final_speed = 100 * final_result_speed



    result_above_reaction = float(char_a_reaction_input_converted_2) - float(char_b_reaction_input_converted_2)
    result_above_reaction_2 = abs(result_above_reaction)

    result_below_reaction = (float(char_a_reaction_input_converted_2) + float(char_b_reaction_input_converted_2))/2
    result_below_reaction_2 = abs(result_below_reaction)

    final_result_reaction = result_above_reaction_2/result_below_reaction_2
    percentage_final_reaction = 100 * final_result_reaction



    result_above_lifting = float(char_a_lifting_input_converted_2) - float(char_b_lifting_input_converted_2)
    result_above_lifting_2 = abs(result_above_lifting)

    result_below_lifting = (float(char_a_lifting_input_converted_2) + float(char_b_lifting_input_converted_2))/2
    result_below_lifting_2 = abs(result_below_lifting)

    final_result_lifting = result_above_lifting_2/result_below_lifting_2
    percentage_final_lifting = 100 * final_result_lifting



    result_above_striking = float(char_a_striking_input_converted_2) - float(char_b_striking_input_converted_2)
    result_above_striking_2 = abs(result_above_striking)

    result_below_striking = (float(char_a_striking_input_converted_2) + float(char_b_striking_input_converted_2))/2
    result_below_striking_2 = abs(result_below_striking)

    final_result_striking = result_above_striking_2/result_below_striking_2
    percentage_final_striking = 100 * final_result_striking



    result_above_durability = float(char_a_durability_input_converted_2) - float(char_b_durability_input_converted_2)
    result_above_durability_2 = abs(result_above_durability)

    result_below_durability = (float(char_a_durability_input_converted_2) + float(char_b_durability_input_converted_2))/2
    result_below_durability_2 = abs(result_below_durability)

    final_result_durability = result_above_durability_2/result_below_durability_2
    percentage_final_durability = 100 * final_result_durability


    name_a = char_a_name_input.get('1.0',"end-1c")
    name_b = char_b_name_input.get('1.0',"end-1c")


    if(float(char_a_attack_potency_input_converted_2) > float(char_b_attack_potency_input_converted_2) and percentage_final_ap > 10):
        part_1="The attack potency of " + name_a + " is higher by " + str(round(percentage_final_ap,2)) + "%. "

    elif(float(char_a_attack_potency_input_converted_2) < float(char_b_attack_potency_input_converted_2) and percentage_final_ap > 10):
        part_1="The attack potency of " + name_b + " is higher by " + str(round(percentage_final_ap,2)) + "%. "

    else:
        part_1="No noticeable difference in attack potency, it is less than 10%. "


    if(float(char_a_speed_input_converted_2) > float(char_b_speed_input_converted_2) and percentage_final_speed > 10):
        part_2="The speed of " + name_a + " is higher by " + str(round(percentage_final_speed,2)) + "%. "

    elif(float(char_a_speed_input_converted_2) < float(char_b_speed_input_converted_2) and percentage_final_speed > 10):
        part_2="The speed of " + name_b + " is higher by " + str(round(percentage_final_speed,2)) + "%. "

    else:
        part_2="No noticeable difference in speed, it is less than 10%. "


    if(float(char_a_reaction_input_converted_2) > float(char_b_reaction_input_converted_2) and percentage_final_reaction > 10):
        part_3="The reaction speed of " + name_a + " is higher by " + str(round(percentage_final_reaction,2)) + "%. "

    elif(float(char_a_reaction_input_converted_2) < float(char_b_reaction_input_converted_2) and percentage_final_reaction > 10):
        part_3="The reaction speed of " + name_b + " is higher by " + str(round(percentage_final_reaction,2)) + "%. "

    else:
        part_3="No noticeable difference in reaction speed, it is less than 10%. "


    if(float(char_a_lifting_input_converted_2) > float(char_b_lifting_input_converted_2) and percentage_final_lifting > 10):
        part_4="The lifting strength of " + name_a + " is higher by " + str(round(percentage_final_lifting,2)) + "%. "

    elif(float(char_a_lifting_input_converted_2) < float(char_b_lifting_input_converted_2) and percentage_final_lifting > 10):
        part_4="The lifting strength of " + name_b + " is higher by " + str(round(percentage_final_lifting,2)) + "%. "

    else:
        part_4="No noticeable difference in lifting strength, it is less than 10%. "


    if(float(char_a_striking_input_converted_2) > float(char_b_striking_input_converted_2) and percentage_final_striking > 10):
        part_5="The striking strength of " + name_a + " is higher by " + str(round(percentage_final_striking,2)) + "%. "

    elif(float(char_a_striking_input_converted_2) < float(char_b_striking_input_converted_2) and percentage_final_striking > 10):
        part_5="The striking strength of " + name_b + " is higher by " + str(round(percentage_final_striking,2)) + "%. "

    else:
        part_5="No noticeable difference in striking strength, it is less than 10%. "


    if(float(char_a_durability_input_converted_2) > float(char_b_durability_input_converted_2) and percentage_final_durability > 10):
        part_8="The durability of " + name_a + " is higher by " + str(round(percentage_final_durability,2)) + "%. "

    elif(float(char_a_durability_input_converted_2) < float(char_b_durability_input_converted_2) and percentage_final_durability > 10):
        part_8="The durability of " + name_b + " is higher by " + str(round(percentage_final_durability,2)) + "%. "

    else:
        part_8="No noticeable difference in durability, it is less than 10%. "


    #More complicated stuff
    #Reaction speed B vs speed of A
    result_above_reaction_b_vs_speed_a = float(char_b_reaction_input_converted_2) - float(char_a_speed_input_converted_2)
    result_below_reaction_b_vs_speed_a = (float(char_b_reaction_input_converted_2) + float(char_a_speed_input_converted_2))/2
    result_above_reaction_b_vs_speed_a_2 = abs(result_above_reaction_b_vs_speed_a)
    result_below_reaction_b_vs_speed_a_2 = abs(result_below_reaction_b_vs_speed_a)

    print("A vs B ABOVE ", result_above_reaction_b_vs_speed_a_2)
    print("A vs B BELOW ", result_below_reaction_b_vs_speed_a_2)

    result_above_below_reaction_b_vs_speed_a = result_above_reaction_b_vs_speed_a_2/result_below_reaction_b_vs_speed_a_2
    result_above_below_reaction_b_vs_speed_a_2 = result_above_below_reaction_b_vs_speed_a * 100

    print("RESULT A VS B ", result_above_below_reaction_b_vs_speed_a_2)

    #Reaction speed A vs speed of B
    result_above_reaction_a_vs_speed_b = float(char_b_reaction_input_converted_2) - float(char_a_speed_input_converted_2)
    result_below_reaction_a_vs_speed_b = (float(char_b_reaction_input_converted_2) + float(char_a_speed_input_converted_2))/2
    result_above_reaction_a_vs_speed_b_2 = abs(result_above_reaction_a_vs_speed_b)
    result_below_reaction_a_vs_speed_b_2 = abs(result_below_reaction_a_vs_speed_b)

    result_above_below_reaction_a_vs_speed_b = result_above_reaction_a_vs_speed_b_2/result_below_reaction_a_vs_speed_b_2
    result_above_below_reaction_a_vs_speed_b_2 = result_above_below_reaction_a_vs_speed_b * 100

    #Compare AP A vs Dura B
    result_above_ap_a_vs_durability_b = float(char_a_attack_potency_input_converted_2) - float(char_b_durability_input_converted_2)
    result_below_ap_a_vs_durability_b = (float(char_a_attack_potency_input_converted_2) + float(char_b_durability_input_converted_2))/2
    result_above_ap_a_vs_durability_b_2 = abs(result_above_ap_a_vs_durability_b)
    result_below_ap_a_vs_durability_b_2 = abs(result_below_ap_a_vs_durability_b)

    result_above_below_ap_a_vs_durability_b = result_above_ap_a_vs_durability_b_2/result_below_ap_a_vs_durability_b_2
    result_above_below_ap_a_vs_durability_b_2 = result_above_below_ap_a_vs_durability_b * 100


    #Compare AP B vs Dura A
    result_above_ap_b_vs_durability_a = float(char_b_attack_potency_input_converted_2) - float(char_a_durability_input_converted_2)
    result_below_ap_b_vs_durability_a = (float(char_b_attack_potency_input_converted_2) + float(char_a_durability_input_converted_2))/2
    result_above_ap_b_vs_durability_a_2 = abs(result_above_ap_b_vs_durability_a)
    result_below_ap_b_vs_durability_a_2 = abs(result_below_ap_b_vs_durability_a)

    result_above_below_ap_b_vs_durability_a = result_above_ap_b_vs_durability_a_2/result_below_ap_b_vs_durability_a_2
    result_above_below_ap_b_vs_durability_a_2 = result_above_below_ap_b_vs_durability_a * 100

    #Compare reaction speed of a and b
    if(float(char_a_speed_input_converted_2) > float(char_b_reaction_input_converted_2) and result_above_below_reaction_a_vs_speed_b_2 > 10):
        part_6="The running speed of " + name_a + " is higher than the reaction speed of " + name_b + " by " + str(round(result_above_below_reaction_a_vs_speed_b_2,2)) + "%. "

    elif(float(char_a_speed_input_converted_2) < float(char_b_reaction_input_converted_2) and result_above_below_reaction_a_vs_speed_b_2 > 10):
        part_6="The running speed of " + name_a + " is lower than the reaction speed of " + name_b + " by " + str(round(result_above_below_reaction_a_vs_speed_b_2,2)) + "%. "

    else:
        part_6="The running speed of " + name_a + " is not that high compared to the reaction speed of " + name_b + ". "

#Compare reaction speed of b and a
    if(float(char_b_speed_input_converted_2) > float(char_a_reaction_input_converted_2) and result_above_below_reaction_b_vs_speed_a_2 > 10):
        part_7="The running speed of " + name_b + " is higher than the reaction speed of " + name_a + " by " + str(round(result_above_below_reaction_b_vs_speed_a_2,2)) + "%. "

    elif(float(char_b_speed_input_converted_2) < float(char_a_reaction_input_converted_2) and result_above_below_reaction_b_vs_speed_a_2 > 10):
        part_7="The running speed of " + name_b + " is lower than the reaction speed of " + name_a + " by " + str(round(result_above_below_reaction_b_vs_speed_a_2,2)) + "%. "

    else:
        part_7="The running speed of " + name_b + " is not that high compared to the reaction speed of " + name_a + ". "

    #Compare Attack Potency A vs Durability B
    if(float(char_a_attack_potency_input_converted_2) > float(char_b_durability_input_converted_2) and result_above_below_ap_a_vs_durability_b_2 > 10):
        part_9="The attack potency of " + name_a + " is higher than the durabiliy of " + name_b + " by " + str(round(result_above_below_ap_a_vs_durability_b_2)) + "%. "

    elif(float(char_a_attack_potency_input_converted_2) < float(char_b_durability_input_converted_2) and result_above_below_ap_a_vs_durability_b_2 > 10):
        part_9="The attack potency of " + name_a + " is way lower than the durabiliy of " + name_b + " by " + str(round(result_above_below_ap_a_vs_durability_b_2)) + "%. "

    else:
        part_9="The attack potency of " + name_a + " is not that high compared to the durability of " + name_b + ". "


    #Compare Attack Potency B vs Durability A
    if(float(char_b_attack_potency_input_converted_2) > float(char_a_durability_input_converted_2) and result_above_below_ap_b_vs_durability_a_2 > 10):
        part_10="The attack potency of " + name_b + " is higher than the durabiliy of " + name_a + " " + str(round(result_above_below_ap_a_vs_durability_b_2)) + "%. "

    elif(float(char_b_attack_potency_input_converted_2) < float(char_a_durability_input_converted_2) and result_above_below_ap_b_vs_durability_a_2 > 10):
        part_10="The attack potency of " + name_b + " is way lower than the durabiliy of " + name_a + " " + str(round(result_above_below_ap_a_vs_durability_b_2)) + "%. "

    else:
        part_10="The attack potency of " + name_b + " is not that high compared to the durability of " + name_a + ". "


    part_final = part_1 + part_2 + part_3 + part_4 + part_8
    part_final_deep = part_6 + part_7 + part_9 + part_10
    messagebox.showinfo(message=part_final, title="Result")
    messagebox.showinfo(message=part_final_deep, title="Result")


btn = Button(root, text="Calculate",command=results)

char_a_name.place(x=0, y=0)
char_a_name_input.place(x=0, y=20)
char_b_name.place(x=300, y=0)
char_b_name_input.place(x=300, y=20)

char_a_attack_potency_label.place(x=0, y=40)
char_a_attack_potency_input.place(x=0, y=60)
char_b_attack_potency_label.place(x=300, y=40)
char_b_attack_potency_input.place(x=300, y=60)

char_a_speed_label.place(x=0, y=80)
char_a_speed_input.place(x=0, y=100)
char_b_speed_label.place(x=300, y=80)
char_b_speed_input.place(x=300, y=100)

char_a_reaction_label.place(x=0, y=120)
char_a_reaction_input.place(x=0, y=140)
char_b_reaction_label.place(x=300, y=120)
char_b_reaction_input.place(x=300, y=140)

char_a_lifting_label.place(x=0, y=160)
char_a_lifting_input.place(x=0, y=180)
char_b_lifting_label.place(x=300, y=160)
char_b_lifting_input.place(x=300, y=180)

char_a_striking_label.place(x=0, y=200)
char_a_striking_input.place(x=0, y=220)
char_b_striking_label.place(x=300, y=200)
char_b_striking_input.place(x=300, y=220)

char_a_durability_label.place(x=0, y=240)
char_a_durability_input.place(x=0, y=260)
char_b_durability_label.place(x=300, y=240)
char_b_durability_input.place(x=300, y=260)

btn.place(x=250, y=300)
tk.mainloop()
