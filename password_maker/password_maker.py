import tkinter as tk
from tkinter import *
from tkinter import messagebox
import hashlib
import time
from time import sleep
import string
import itertools
import os
import math
import random
from os import system #Os Commands

nombre_os = os.name

#Title
root = tk.Tk()
root.title('Python password strength checker')
root.geometry("400x250")

def helping(): #The help button
    messagebox.showinfo(message="Type your password to generate a md5 hash with the 'TURN TO MD5' button, or put the md5 hash on the textbox below and press 'CRACK MD5'. The process consumes lots of CPU power", title="HELP")

b_help = Button(root, text = "HELP", command=helping)

these_labels = Label(root, text = "Password")
stuff_password = Text(root, height = 2, width = 20)
empty_space =  Label(root, text = "")
md5_lavel = Label(root, text = "MD5 Hash")

def create_md5_file(): #Create a md5 hash
    into_hash = stuff_password.get("1.0",'end-1c') #Grabs everything on the textbox, even the spaces
    turn_into_hash = hashlib.md5(into_hash.encode()) #Turns it intomd5 hash
    hashed_password = turn_into_hash.hexdigest() #Makes the hash into a string
    
    messagebox.showinfo(message=hashed_password, title="Password in hashed form")

    text_grab_password.set(hashed_password) #Equals the StringVar to a textbox


def create_pass(): #Create a md5 hash
    stuff_password.delete('1.0', END)
    char_set = string.ascii_uppercase + string.digits
    result_set = ''.join(random.sample(char_set*6, 6))

    stuff_password.insert(INSERT, result_set)


def cracking(): #The md5 cracker
    hashed_password = textBox_MD5_RESULT.get() #Grabs the tk.Entry with the password
    chars = string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation
    attempts = 0
    done = 0
    print("Working on it")

    start_time = time.time()

    #chars gets all the ascii values, attempts is the number of attempts, done is when it is found

    for password_length in range(1, 36): #This will attempt from a single set to 36 sets
        for guess in itertools.product(chars, repeat=password_length): 
            attempts += 1 #Adds one attempt to 0
            guess = ''.join(guess) #Adds one character if the last possible char is done
            hashing_guess = hashlib.md5(guess.encode()) #Turns the guess to md5
            hashed_guess = hashing_guess.hexdigest() #Turns the md5 into a string

            if hashed_guess == hashed_password: #Compares it and if it is found, it is done
                formated_result = format(attempts, ",") #Formats the attempts to make them legible
                time_it_took = time.time() - start_time
                formated_seconds = str(round(time_it_took,2))

                congrats = "Your hash: " + hashed_password + " took this many seconds to crack: " + formated_seconds + " and this many guesses: " + formated_result + ". Your password is: " + guess
                messagebox.showinfo(message=congrats, title="Password found")
                done = 1

            if(done==1): #If it is done, clears everything and breaks the for loop
                if nombre_os == "posix":
                    system('clear')
                else:
                    system('cls')
                break   

b_MD5 = Button(root, text = "TURN TO MD5", command=create_md5_file)

b_help.pack(side=tk.TOP)
these_labels.pack()
stuff_password.pack()
b_MD5.pack()

b_Create = Button(root, text = "CREATE PASSWORD", command=create_pass)
b_Create.pack()

empty_space.pack()

empty_space.pack()
md5_lavel.pack()

text_grab_password = tk.StringVar()
textBox_MD5_RESULT = tk.Entry(root, textvariable = text_grab_password, width = 50)
textBox_MD5_RESULT.pack()

b3 = Button(root, text = "CRACK MD5", command=cracking)
b3.pack()
tk.mainloop()