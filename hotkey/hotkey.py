import keyboard
import time
import pyautogui

# Using time.sleep, we can dramatically decrease the amount of CPU our program
# uses.

hotkey = "a"
# Remember that the order in which the hotkey is set up is the order you
# need to press the keys.

while True:
  if keyboard.is_pressed(hotkey):
    pyautogui.press("num8")
    time.sleep(0.01)
    pyautogui.press("num4")
    time.sleep(0.01)
    pyautogui.press("num2")
    time.sleep(0.01)
    pyautogui.press("num6")
    time.sleep(0.01)
  time.sleep(0.01)