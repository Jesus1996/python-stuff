from math import *
from time import sleep

print("This will detect if a number is even or odd. Also if it can be divided by four")
checking = int(input("Please put the number you want to check "))
print("")
#This does the division but shows the remainder instead
result_by_two = checking % 2
result_by_four = checking % 4

if(result_by_four == 0):
    print("It is divisible by four")
    sleep(3)

    if(result_by_two == 0):
        print("It is even too")
    else:
        print("It is odd too")

else:
    print("It is not divisible by four")
    sleep(3)

    if(result_by_two == 0):
        print("It is even too")
    else:
        print("It is odd too")