import tkinter as tk
from tkinter import *
from tkinter import messagebox
import hashlib
import time
from time import sleep

#Title
root = tk.Tk()
root.title('EVENT REPEATER')
root.geometry("400x300")

year_used_label = Label(root, text = "Starting Year")
stuff_year_year_used = Text(root, height = 2, width = 20)
empty_space = Label(root, text = "")
end_year_label = Label(root, text = "Ending Year")
empty_space2 = Label(root, text = "")
stuff_year_end = Text(root, height = 2, width = 20)
empty_space3 = Label(root, text = "")

repeat_label = Label(root, text = "Every what years the event repeats")
repeat_year = Text(root, height = 2, width = 20)

def helping(): #The help button
    messagebox.showinfo(message="Type the starting year of your chosing, then type the end year and the number of years said event happens", title="HELP")

def checking(): #The help button
    
    if stuff_year_year_used.get("1.0", END)=="\n":
        messagebox.showerror(message="Initial year is empty", title="Error")
        return
    
    if stuff_year_end.get("1.0", END)=="\n":
        messagebox.showerror(message="Ending year is empty", title="Error")
        return
    
    if repeat_year.get("1.0", END)=="\n":
        messagebox.showerror(message="The event is empty", title="Error")
        return
    
    year_used_string = stuff_year_year_used.get("1.0",'end-1c')
    year_finish_string = stuff_year_end.get("1.0",'end-1c')
    repeats_per_time_string = repeat_year.get("1.0",'end-1c')
    

    year_used = int(year_used_string)
    year_finish = int(year_finish_string)
    repeats_per_time = int(repeats_per_time_string)
    check_if_possible_1 = year_finish - year_used

    the_chain = []

    if(check_if_possible_1 <= repeats_per_time): #If the event is not possible, it outputs this
        messagebox.showinfo(message="The event is not possible, the timeframe is way smaller than the repeatable event", title="ERROR")
    else:
        while year_used <= year_finish: #If the event is possible, it appends the years that fit using this while loop
            the_chain.append(year_used)
            year_used += repeats_per_time #Adds the event plus the starting year and repeats until it can't fit anymore

            if year_used >= year_finish:
                messagebox.showinfo(message=the_chain, title="RESULTS")
                break

b1 = Button(root, text = "HELP", command=helping)

b2 = Button(root, text = "CALCULATE DATE", command=checking)

b1.pack()
year_used_label.pack()
stuff_year_year_used.pack()
empty_space.pack()
end_year_label.pack()
stuff_year_end.pack()
empty_space2.pack()
repeat_label.pack()
repeat_year.pack()
empty_space3.pack()
b2.pack()
tk.mainloop()
