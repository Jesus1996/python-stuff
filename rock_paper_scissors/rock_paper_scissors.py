import random
import time
import os
from time import sleep

def screen_clear():
   # If UNIX 
   if os.name == 'posix':
      _ = os.system('clear')
   else:
      # for windows platfrom
      _ = os.system('cls')
   # print out some text

print("Put rock, paper or scissors to answer \n")
print("You are fightning an AI controlled opponent, it doesn't read your input \n")
user1 = str(input("What's your name? "))

while True:
    user_answer = str(input("Do you want to choose rock, paper or scissors? "))
    user1_answer = user_answer.lower()

    if user1_answer == "rock" or user1_answer == "paper" or user1_answer == "scissors":
        print("You chose:", user1_answer, "\n")
        random_answer = ["rock", "paper", "scissors"]
        ai_values = random.choice(random_answer)
        time.sleep(3)
        print("The computer chose:", ai_values, "\n")

    else:
        print("")
        print("Not a valid answer, quitting")
        break

    #player is rock
    if(user1_answer == 'rock' and ai_values == 'paper'):
        print(user1, "LOST! PAPER COVERS ROCK! \n")
        print("Gonna clear the screen right now")
        sleep(5)
        screen_clear()

    if(user1_answer == 'rock' and ai_values == 'scissors'):
        print(user1, "WON! ROCK DESTROYS SCISSORS! \n")
        print("Gonna clear the screen right now")
        sleep(5)
        screen_clear()

    #player is scissors
    if(user1_answer == 'scissors' and ai_values == 'rock'):
        print(user1, "LOST! ROCK DESTROYS SCISSORS! \n")
        print("Gonna clear the screen right now")
        sleep(5)
        screen_clear()

    if(user1_answer == 'scissors' and ai_values == 'paper'):
        print(user1, "WON! SCISSORS CUT PAPER! \n")
        print("Gonna clear the screen right now")
        sleep(5)
        screen_clear()

    #player is paper
    if(user1_answer == 'paper' and ai_values == 'rock'):
        print(user1, "WON! PAPER COVERS ROCK! \n")
        print("Gonna clear the screen right now")
        sleep(5)
        screen_clear()

    if(user1_answer == 'paper' and ai_values == 'scissors'):
        print(user1, "LOST! SCISSORS CUT PAPER! \n")
        print("Gonna clear the screen right now")
        sleep(5)
        screen_clear()

    if(user1_answer == ai_values):
        print("TIE due to:", user1_answer, "vs", ai_values, "\n")
        print("Gonna clear the screen right now")
        sleep(5)
        screen_clear()