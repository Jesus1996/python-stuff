import tkinter as tk

window = tk.Tk()
label = tk.Label(
    text="The thing below is a button, hover over it",
)

button = tk.Button(
    text="Click me!",
    width=25,
    height=5,
    bg="blue",
    fg="yellow",
)

label.pack()
button.pack()
window.mainloop()