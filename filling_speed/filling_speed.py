import tkinter as tk
from tkinter import *
from tkinter import messagebox
from time import sleep

root = tk.Tk()
root.title('FILLING SPEED')
root.geometry("400x230")

def calcular():
    volumeValue=txt_volume.get("1.0","end-1c")
    timeValue=txt_time.get("1.0","end-1c")
    recipientValue=txt_recipient.get("1.0","end-1c")

    resultValue=int(volumeValue)/int(timeValue)
    resultValue_speed = str(resultValue) + " Liters per second"
    resultValue_3 = int(recipientValue)/resultValue

    if(resultValue_3 > 3600):
        resultValue_4 = resultValue_3/3600
        resultValue_Time = str(resultValue_4) + " Hours"

    else:
        resultValue_Time = str(resultValue_3) + " Seconds"

    messagebox.showinfo(message=resultValue_speed, title="SPEED")
    messagebox.showinfo(message=resultValue_Time, title="TIME")

volume_label = Label(root, text = "VOLUME IN LITRES: ", pady=8)
txt_volume = Text(root, width=20, height=1)
time_label = Label(root, text = "TIME IN SECONDS: ", pady=8)
txt_time = Text(root, width=20, height=1)
recipient_label = Label(root, text = "RECIPIENT VOLUME IN LITRES: ", pady=8)
txt_recipient = Text(root, width=20, height=1)

volume_label.pack()
txt_volume.pack()
time_label.pack()
txt_time.pack()
recipient_label.pack()
txt_recipient .pack()

b1 = Button(
    root, 
    text="CALCULATE", 
    command=calcular,
    width=40
    ).pack(side=TOP, pady=10)
tk.mainloop()