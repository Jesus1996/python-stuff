import random
import os
from time import sleep

def screen_clear():
   # If UNIX 
   if os.name == 'posix':
      _ = os.system('clear')
   else:
      # for windows platfrom
      _ = os.system('cls')
   # print out some text

while True:
  print("This will generate a random list of any number you want to give it \n")
  first_a = int(input("Put the smallest number in the first list: "))
  second_a = int(input("Put the largest number in the first list: "))
  first_b = int(input("Put the smallest number in the second list: "))
  second_b = int(input("Put the largest number in the second list: "))
  size = int(input("Put the max size you want each list to have: "))
  print("")

  #Creates random numbers
  a = random.sample(range(first_a,second_a), size)
  b = random.sample(range(first_b,second_b), size)

  #The overlap compares the b set with the a set
  result_overlap = [i for i in set(a) if i in b]

  #Creates an empty chain which then fills up with the samples
  result = []
  for element in result_overlap:
    if element not in result:
      result.append(element)

  if result == "":
    print("Nothing in common was found")
    break
  else:
    print("Data of the first list:", a)
    print("Data of the second list:", b)
    print("")
    print("Common data:", result)
    print("Gonna clear the screen right now")
    sleep(5)
    screen_clear()