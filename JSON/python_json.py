import json

obj = {
  "Id": 78912,
  "Customer": "Jason Sweet",
  "Quantity": 1,
  "Price": 18.00
}

json_str = json.dumps(obj, indent=4)
      
print(json_str)