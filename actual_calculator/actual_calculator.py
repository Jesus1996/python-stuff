from math import *
import os
from os import system #Os Commands

nombre_os = os.name

if nombre_os == "posix":
    system('clear')
else:
    system('cls')


print("Please put a valid syntax. This doesn't accept percentages")


functions = {'__builtins__': None}
variables = {'__builtins__': None}

#All the functions it can recognize
functions = {'acos': acos,
            'asin': asin,
            'atan': atan,
            'atan2': atan2,
            'ceil': ceil,
            'cos': cos,
            'cosh': cosh,
            'degrees': degrees,
            'exp': exp,
            'fabs':fabs,
            'floor': floor,
            'fmod': fmod,
            'frexp': frexp,
            'hypot': hypot,
            'ldexp': ldexp,
            'log': log,
            'log10': log10,
            'modf': modf,
            'pow': pow,
            'radians': radians,
            'sin': sin,
            'sinh': sinh,
            'sqrt': sqrt,
            'tan': tan,
            'tanh': tanh}

variables = {'e': e, 'pi': pi}

#Detect if something went wrong
while True:
    math_expression = input("Enter a math expression: ")
    #Remove the commas
    string_dup=math_expression.replace(',',"")
    try:
        eval(string_dup, variables, functions)
    except (SyntaxError, NameError, ZeroDivisionError):
        print("The syntax is not valid or asked something impossible, quitting")
        break
    else:
        result = eval(string_dup)
        formated_result = format(result, ",")
        print("The syntax is valid \n")
        print("Result of", math_expression, "is:", formated_result)
        print("")
#Just in case it shows up as decimals, shows them