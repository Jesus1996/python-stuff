import random 
from time import sleep
print("This is an algoritm that will randomize a number, you have to guess it")
print("The numbers are intergers from 0 to 9, so you have a chance of 10% to guess it \n")
user_answer = int(input("Put your number from 0 to 9 \n"))

if user_answer < 0 or user_answer > 9:
    print("Your number is less than 0 or more than 9, this is invalid")

else:
    sleep(3)
    number_generated = random.randint(0, 9)
    print("The algorithm chose the following number: ", number_generated)

    if(user_answer is number_generated):
        print("You won!")
    else:
        print("You failed!")