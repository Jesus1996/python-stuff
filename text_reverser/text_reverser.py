## initializing the string
print("You want to reverse the sentence or the words?")
print("If you want to reverse the sentence, it will be like this: I AM becomes AM I")
print("If you want to reverse the words, it will be like this: I AM becomes MA I")
selection = input("Press 1 to reverse words, press any other key to reverse the sentence: ")

if selection == '1':

    string = input("Put your text here: ")
    ## splitting the string on space
    words = string.split()
    ## reversing the words using reversed() function
    words = list(reversed(words))
    ## joining the words and printing
    print(" ".join(words))

else:
     text = input("Put your text here: ")
     reversion = text[::-1]
     print(reversion)

