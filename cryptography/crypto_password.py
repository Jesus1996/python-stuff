import tkinter as tk
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
from time import sleep
import os
from os import system #Os Commands
import codecs

nombre_os = os.name

root = tk.Tk()
root.title('CRYPTO PASSWORD')
root.geometry("400x450")

cwd = os.getcwd()

def helping():
    messagebox.showinfo(message="Press the buttons according to what you want to do", title="HELP")

def openFile(): #Abrir un dialogo
    tf = filedialog.askopenfilename(
        initialdir=cwd, 
        title="Open Text file", 
        filetypes=(("Text Files", "*.txt"),)
        )
    txtarea.delete('1.0', END)
    pathh.insert(END, tf)
    tf = open(tf)  #Meter los datos a txtarea
    data = tf.read()
    txtarea.insert(END, data)
    tf.close()

def encrypt():
    inputValue=txtarea.get("1.0","end-1c")
    txtarea.delete('1.0', END)
    codigo = codecs.encode(inputValue, 'rot_13')
    txtarea.insert(END, codigo)

def guardar():
    f = filedialog.asksaveasfile(mode='w', defaultextension=".txt")
    if f is None: # asksaveasfile return `None` if dialog closed with "cancel".
        return
    text2save = str(txtarea.get(1.0, END)) # starts from `1.0`, not `0.0`
    f.write(text2save)
    f.close() # `()` was missing.

txtarea = Text(root, width=40, height=10)
pathh = Text(root, width=40, height=1)

b1 = Button(root, text = "HELP ", command=helping)


path_label = Label(root, text = "Path: ")


textarea_label = Label(root, text = "Content: ", pady=8)

b1.pack(side=TOP)

path_label.pack()
pathh.pack()
path_label.pack()

b2 = Button(
    root, 
    text="Open File", 
    command=openFile,
    width=40
    ).pack(side=TOP, pady=10)


textarea_label.pack()
txtarea.pack()

b3 = Button(
    root, 
    text="ENCRYPT/DECRYPT", 
    command=encrypt,
    width=40
    ).pack(side=TOP, pady=10)


b5 = Button(
    root, 
    text="SAVE", 
    command=guardar,
    width=40
    ).pack(side=TOP, pady=10)
tk.mainloop()